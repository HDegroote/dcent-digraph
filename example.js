const { BaseNode, DiGraph } = require('.')

// Define your Node (implementing getChildren and uniqueId)
// In this example we simulate the decentralised nature by explicitly pausing in getChildren
// --in a normal implementation, the Node gets its children from elsewhere
// (each child could come from a different location even)
class SimpleNode extends BaseNode {
  static UNIQUE_ID = 0

  constructor (content = '', children = [], { msNetworkDelay = 100 } = {}) {
    super(SimpleNode.UNIQUE_ID++)
    this.content = content
    this.children = children
    this.msNetworkDelay = msNetworkDelay
  }

  async getChildren () {
    await new Promise(resolve => setTimeout(resolve, this.msNetworkDelay))
    return this.children
  }
}

async function main () {
  const node1 = new SimpleNode('node1')
  const node2 = new SimpleNode('node2')
  const root = new SimpleNode('rootContent', [node1, node2])

  // A DiGraph is fully defined by its rootNode:
  const diGraph = new DiGraph(root)

  // Now you can use the implemented algorithms:
  console.log('Efficiently yield all nodes from the di-graph (note: no order guarantees)')
  for await (const node of diGraph.yieldAllNodesOnce()) {
    console.log(node.uniqueId, ': ', node.content)
  }

  console.log('\nWalk the di-graph depth-first')
  for await (const node of diGraph.walkDepthFirst()) {
    console.log(node.uniqueId, ': ', node.content)
  }
}

main()
