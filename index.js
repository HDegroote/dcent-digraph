const safetyCatch = require('safety-catch')

class BaseNode {
  constructor (uniqueId) {
    if (uniqueId == null) throw new Error('uniqueId must be specified')

    this.uniqueId = uniqueId
  }

  async getChildren () { throw new Error('getChildren not implemented') }
}

class DiGraph {
  constructor (rootNode) {
    this.rootNode = rootNode
  }

  async * walkDepthFirst () {
    // Note: can be infinite generator if the digraph contains a loop
    const stepper = await this.recStepper()
    yield * this._yieldFromStepperDepthFirst(stepper)
  }

  async * _yieldFromStepperDepthFirst ({ node, childYielders }) {
    yield node

    // Already start resolving all children
    const childPromises = childYielders.map((yieldChild) => {
      const promise = yieldChild()
      promise.catch(safetyCatch) // Silently swallow all errors
      return promise
    })

    for (const childPromise of childPromises) {
      const nextStep = await childPromise
      yield * this._yieldFromStepperDepthFirst(nextStep)
    }
  }

  async recStepper (node) {
    node = node ?? this.rootNode

    const childYielders = []
    for await (const child of (await node.getChildren())) {
      const yieldChild = async () => await this.recStepper(child)
      childYielders.push(yieldChild)
    }

    return { node, childYielders }
  }

  async * yieldAllNodesOnce () {
    // Walks the di-graph starting from the root, and yields
    // each node once, without guaranteeing any order.
    // If a node is reachable from multiple paths,
    // several may reach that node, but only the first
    // one to reach it will yield it and continue with its children
    // --children are resolved 'simultaneously' in async sense
    // (the algorithm does not descend child per child, but requests all
    // and yields nodes in the order in which they are resolved)

    yield * this._yieldAllNodesOnceRec(
      { visitedEntries: new Set(), node: this.rootNode }
    )
  }

  async * _yieldAllNodesOnceRec ({ visitedEntries, node }) {
    // DEVNOTE: all recursive invocations share
    // the same visitedEntries set. There must not be any
    // await between the check on whether it contains an entry,
    // and adding that entry to it. Otherwise entries might be visited
    // multiple times.
    if (visitedEntries.has(node.uniqueId)) return
    visitedEntries.add(node.uniqueId)

    yield node

    const childGenerators = []
    const allPromises = {}
    // getChildren can be an async iterator, or return an iterable
    for await (const child of (await node.getChildren())) {
      const childGenerator = this._yieldAllNodesOnceRec({ visitedEntries, node: child })
      childGenerators.push(childGenerator)

      // DEVNOTE: i is wrapped in promise as well, to know upon resolution
      // what the corresponding childGenerator index was
      const i = childGenerators.length - 1
      allPromises[i] = Promise.all([i, childGenerator.next()])
      allPromises[i].catch(safetyCatch)
    }

    // End-condition is when allPromises consists only of deleted (empty) entries
    while (Object.keys(allPromises).length > 0) {
      const [winnerIndex, winner] = await Promise.race(Object.values(allPromises))

      if (winner.done === true) {
        delete allPromises[winnerIndex]
      } else {
        allPromises[winnerIndex] = Promise.all(
          [winnerIndex, childGenerators[winnerIndex].next()]
        )
        allPromises[winnerIndex].catch(safetyCatch)
        yield winner.value
      }
    }
  }
}

module.exports = { DiGraph, BaseNode }
