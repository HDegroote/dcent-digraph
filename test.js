const { expect } = require('chai')
const { strict: nodeAssert } = require('assert')

const { BaseNode, DiGraph } = require('.')

describe('Test digraph', function () {
  let diGraph
  let node1, node2, root
  const MS_NETWORK_DELAY = 100

  this.beforeEach(async function () {
    node1 = new SimpleNode('node1', [], { MS_NETWORK_DELAY })
    node2 = new SimpleNode('node2', [], { MS_NETWORK_DELAY })
    root = new SimpleNode('rootContent', [node1, node2], { MS_NETWORK_DELAY })

    diGraph = new DiGraph(root)
  })

  it('Throws when no uniqueId is specified', async function () {
    class BadNode extends BaseNode {
      async getChildren () {
        return []
      }
    }

    expect(() => new BadNode()).to.throw()
  })

  it('Throws on not implemented getChildren', async function () {
    class BadNode extends BaseNode {
      constructor () {
        super(0)
      }
    }

    const badGraph = new DiGraph(new BadNode())

    await nodeAssert.rejects(
      _getAllNodes(badGraph),
      { message: 'getChildren not implemented' }
    )
  })

  it('Can yield all nodes once', async function () {
    const allNodes = await _getAllNodes(diGraph)

    const expectedContents = new Set(['node1', 'node2', 'rootContent'])
    expect(allNodes.length).to.equal(expectedContents.size)

    const actualContents = await _getNodeContentSet(allNodes)
    expect(expectedContents).to.deep.equal(actualContents)
  })

  it('Can yield all nodes once with repeated entries', async function () {
    node2.children.push(node1)

    const allNodes = await _getAllNodes(diGraph)

    const expectedContents = new Set(['node1', 'node2', 'rootContent'])
    expect(allNodes.length).to.equal(expectedContents.size)

    const actualContents = await _getNodeContentSet(allNodes)
    expect(expectedContents).to.deep.equal(actualContents)
  })

  it('walks steps as expected', async function () {
    const step1 = await diGraph.recStepper()
    expect(step1.node.content).to.equal(root.content)
    expect(step1.childYielders.length).to.equal(2)

    const step2a = await step1.childYielders[0]()
    expect(await step2a.node.content).to.equal(node1.content)
    expect(step2a.childYielders.length).to.equal(0)

    const step2b = await step1.childYielders[1]()
    expect(step2b.node.content).to.equal(node2.content)
    expect(step2b.childYielders.length).to.equal(0)
  })

  it('Walks depth first correctly', async function () {
    const node3 = new SimpleNode('Content3')
    node1.children.push(node3)

    const node4 = new SimpleNode('Content4', [node3, node3])
    node1.children.push(node4)

    const contents = []
    for await (const node of diGraph.walkDepthFirst()) {
      contents.push(node.content)
    }
    expect(contents).to.deep.equal([
      root.content, node1.content, node3.content, node4.content, node3.content, node3.content, node2.content
    ])
  })

  it('visits the same node multiple times when walking depth first', async function () {
    node2.children.push(node1)

    const contents = []
    for await (const node of diGraph.walkDepthFirst()) {
      contents.push(node.content)
    }
    expect(contents.length).to.equal(4)
    expect(contents).to.deep.equal([root.content, node1.content, node2.content, node1.content])
  })

  it('follows loops when walking depth first', async function () {
    node2.children.push(node1)

    node1.children.push(node2)

    const nodes = []
    const maxVisits = 20
    let nrVisits = 0
    for await (const node of diGraph.walkDepthFirst()) {
      nodes.push(node)
      nrVisits += 1
      if (nrVisits >= maxVisits) {
        break
      }
    }

    const contents = nodes.map((n) => n.content)
    expect(contents.length).to.equal(maxVisits)
    // Loop of size 2, so every second entry is equal
    expect(
      contents[maxVisits - 1]).to.not.equal(contents[maxVisits - 2]
    ) // Sanity check
    expect(
      (new Set([contents[maxVisits - 1], contents[maxVisits - 3], contents[maxVisits - 5]])).size
    ).to.equal(1)
    expect(
      (new Set([contents[maxVisits - 2], contents[maxVisits - 4], contents[maxVisits - 6]])).size
    ).to.equal(1)
  })

  describe('Test async parallellism', function () {
    it('Simultaneously awaits all children when walking all nodes once', async function () {
      for (let i = 3; i < 10; i++) {
        const node = new SimpleNode(`Content ${i}`)
        root.children.push(node)
      }

      await setDelayRec(MS_NETWORK_DELAY, diGraph)

      const startTime = new Date()
      const allNodes = await _getAllNodes(diGraph)
      const timeMs = new Date() - startTime

      // non-simultaneous exec would be > 10*100ms
      // Underbound here is 2*100 (root->entry-x)
      expect(timeMs).to.be.lessThan(5 * MS_NETWORK_DELAY)
      expect(allNodes.length).to.equal(10) // Sanity check
    })

    it('Simultaneous awaits when walking all nodes once -- complex scenario', async function () {
      node1.children = [new SimpleNode(), new SimpleNode(), new SimpleNode()]

      const node2LastChild = new SimpleNode('', [new SimpleNode(), new SimpleNode()])
      node2.children = [new SimpleNode(), new SimpleNode(), node2LastChild]

      await setDelayRec(MS_NETWORK_DELAY, diGraph)

      const startTime = new Date()
      const allNodes = await _getAllNodes(diGraph)
      const timeMs = new Date() - startTime

      // non-simultaneous exec would be > 11*100ms
      // Best possible here is 4*100ms (root->entry2->entry2child3->lvl3Child2)
      expect(timeMs).to.be.lessThan(7 * MS_NETWORK_DELAY) // Some margin
      expect(allNodes.length).to.equal(11) // Sanity check
    })

    it('walkDepthFirst starts resolving siblings simultaneously', async function () {
      for (let i = 3; i < 10; i++) {
        root.children.push(new SimpleNode())
      }

      await setDelayRec(MS_NETWORK_DELAY, diGraph)

      const allNodes = []
      const startTime = new Date()
      for await (const node of diGraph.walkDepthFirst()) {
        allNodes.push(node)
      }
      const timeMs = new Date() - startTime

      expect(allNodes.length).to.equal(10)
      // 1 delay for root, simultaneous delays for all other nodes
      expect(timeMs).to.be.lessThan(MS_NETWORK_DELAY * 4)
    })
  })

  describe('Error handling', function () {
    it('walkDepthFirst stops iterating upon an error and later errors are swallowed', async function () {
      root.children.push(new ErrorNode('first oops'))
      root.children.push(new SimpleNode())
      root.children.push(new ErrorNode('second oops'))
      root.children.push(new SimpleNode())

      node1.children.push(new ErrorNode('child1 oops'))

      const allNodes = []
      let errMsg
      try {
        for await (const node of diGraph.walkDepthFirst()) {
          allNodes.push(node)
        }
      } catch (err) {
        errMsg = err.message
      }

      expect(errMsg).to.equal('child1 oops')
      expect(await getNodeContentList(allNodes)).to.deep.equal(['rootContent', 'node1'])
    })

    it('yieldAllNodesOnce stops iterating upon an error and swallows later errors', async function () {
      root.children.push(new ErrorNode('first oops'))
      root.children.push(new SimpleNode('normal node1'))
      root.children.push(new ErrorNode('second oops'))
      root.children.push(new SimpleNode('normal node2'))

      node1.children.push(new ErrorNode('child1 oops'))
      node2.children.push(new ErrorNode('child1 oops2'))

      const allNodes = []
      let errMsg
      try {
        for await (const node of diGraph.yieldAllNodesOnce()) {
          allNodes.push(node)
        }
      } catch (err) {
        errMsg = err.message
      }

      expect(errMsg).to.equal('first oops')
      // Note: yielding of the node happens before resolving the children,
      // so the error node is still yielded (unlike with depth-first walk)
      expect(await getNodeContentList(allNodes)).to.deep.equal(['rootContent', 'node1', 'node2', ''])
    })
  })

  it('Can traverse the graph with async iterator for getChildren', async function () {
    node1 = new AsyncIterNode('node1', [], { MS_NETWORK_DELAY })
    node2 = new AsyncIterNode('node2', [], { MS_NETWORK_DELAY })
    root = new AsyncIterNode('rootContent', [node1, node2], { MS_NETWORK_DELAY })

    const diGraph = new DiGraph(root)

    {
      // yieldAllNodesOnce
      const allNodes = await _getAllNodes(diGraph)

      const expectedContents = new Set(['node1', 'node2', 'rootContent'])
      expect(allNodes.length).to.equal(expectedContents.size)

      const actualContents = await _getNodeContentSet(allNodes)
      expect(expectedContents).to.deep.equal(actualContents)
    }

    {
      // recursiveStepper
      const contents = []
      for await (const node of diGraph.walkDepthFirst()) {
        contents.push(node.content)
      }
      expect(contents).to.deep.equal([
        root.content, node1.content, node2.content
      ])
    }
  })
})

class SimpleNode extends BaseNode {
  static UNIQUE_ID = 0

  constructor (content = '', children = [], { msNetworkDelay = 0 } = {}) {
    super(SimpleNode.UNIQUE_ID++)
    this.content = content
    this.children = children
    this.msNetworkDelay = msNetworkDelay
  }

  async getChildren () {
    await new Promise(resolve => setTimeout(resolve, this.msNetworkDelay))
    return this.children
  }
}

class ErrorNode extends SimpleNode {
  constructor (msg) {
    super()
    this.msg = msg
  }

  async getChildren () {
    throw new Error(this.msg)
  }
}

class AsyncIterNode extends BaseNode {
  static UNIQUE_ID = 0

  constructor (content = '', children = [], { msNetworkDelay = 0 } = {}) {
    super(AsyncIterNode.UNIQUE_ID++)
    this.content = content
    this.children = children
    this.msNetworkDelay = msNetworkDelay
  }

  async * getChildren () {
    for (const child of this.children) {
      await new Promise(resolve => setTimeout(resolve, this.msNetworkDelay))
      yield child
    }
  }
}

async function setDelayRec (msNetworkDelay, diGraph) {
  for await (const node of diGraph.yieldAllNodesOnce()) {
    node.msNetworkDelay = msNetworkDelay
  }
}

async function _getAllNodes (diGraph) {
  const allNodes = []
  for await (const node of diGraph.yieldAllNodesOnce()) {
    allNodes.push(node)
  }
  return allNodes
}

async function _getNodeContentSet (nodes) {
  return new Set(await getNodeContentList(nodes))
}

async function getNodeContentList (nodes) {
  return nodes.map((node) => node.content)
}
